const express = require("express");
const mongoose = require("mongoose");

const port = 3001;
const app = express();

// [Section] MongoDB connection
	//Syntax:
		//mongoose.connect("mongoDBconnectionString",{options to avoide errors in our connections})

mongoose.connect("mongodb+srv://admin:admin@batch245-dagon.wirp2aj.mongodb.net/s35-discussion?retryWrites=true&w=majority",

{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
let db = mongoose.connection;

//error handling in connecting
db.on("error", console.error.bind(console.log,"Connection error"));

//this will be triggered if the connection is succesful
db.once("open", () => console.log("We are connected to the cloud database"));


// Mongoose  Schemas
	// Schemas determin the structure of the documents to be written in the database
	//Schemas act as the blueprint to our data
	//Syntax:
		//const schemaName = new mongoose.Schema({keyvaluepairs});

const userSchema = new mongoose.Schema({
		username :{
		type: String,
		required:[true, "user name is required"]
			},
		password :{
		type: String,
		required:[true, "user password is required"]
			},
		
});









// const taskSchema = new mongoose.Schema({
// 	name :{
// 		type: String,
// 		required:[true, "Task name is required"]
// 			},
// 	status:{
// 		type : String,
// 		default: "pending"
// 	}
// });


//[Section] models
	//Syntax:
		//const varaibleName = mongoose.model("collectionName",schemaName);

// const Task = mongoose.model("Task", taskSchema);
const Task = mongoose.model("Task", userSchema);


//middlewares


app.use(express.json());
//it will allow oyr app to read data from forms
app.use(express.urlencoded({extended: true}))

//[Section] Routing

	//Create/add new task
		// check if the task is existing.
				//if task already exist in the database, we will return message "The task is already existing!"
				//if the task doesnt exist in the database, we will add it in the database.


/*app.post("/tasks",(request, response)=>{

	let input = request.body
	console.log(input.status);
	if(input.status === undefined){

		Task.findOne({name: input.name},(error, result)=> {


		console.log(result)
		
		if(result!== null){
			return response.send("the task is already existing!")
		}else{
			let newTask = new Task({
				name: input.name
			});
			// save() method will save the object in the collection that the object instatiated
			newTask.save((saveError, savedTask)=>{
				if(saveError){
					return console.log(saveError);
				}else{
					return response.send("New Task created!");
				}
			})
		}

		});

			}else{
			Task.findOne({name: input.name},(error, result)=> {


			console.log(result)
			
			if(result!== null){
				return response.send("the task is already existing!")
			}else{
				let newTask = new Task({
					name: input.name,
					status: input.status
				});
				// save() method will save the object in the collection that the object instatiated
				newTask.save((saveError, savedTask)=>{
					if(saveError){
						return console.log(saveError);
					}else{
						return response.send("New Task created!");
					}
				})
			}

			});

			}
		});*/
	

//[Section] Retrieving the all 

app.get("/tasks",(request,response)=>{

		Task.find({},(error, result)=>{
			if(error){
				console.log(error);
			}else{
				return response.send(result);
			}
		})

})

// SIGN UP
app.post("/signup",(request, response)=>{

	let input = request.body
	console.log(input.status);
	if(input.status === undefined){

		Task.findOne({username: input.username},(error, result)=> {


		console.log(result)
		
		if(result!== null){
			return response.send("the user is already existing!")
		}else{
			let newUser = new Task({
				username: input.username,
				password: input.password
			});
			// save() method will save the object in the collection that the object instatiated
			newUser.save((saveError, savedUser)=>{
				if(saveError){
					return console.log(saveError);
				}else{
					return response.send("New User created!");
				}
			})
		}

		});

			}else{
			Task.findOne({username: input.username},(error, result)=> {


			console.log(result)
			
			if(result!== null){
				return response.send("the user is already existing!")
			}else{
				let newUser = new User({
					username: input.username,
					password: input.password
				});
				// save() method will save the object in the collection that the object instatiated
				newUser.save((saveError, savedUser)=>{
					if(saveError){
						return console.log(saveError);
					}else{
						return response.send("New user created!");
					}
				})
			}

			});

			}
		});
app.get("/signup",(request,response)=>{

		Task.find({},(error, result)=>{
			if(error){
				console.log(error);
			}else{
				return response.send(result);
			}
		})

})
app.listen(port,()=> console.log(`Server is running at port ${port}`));

